package engine

import (
	"errors"
	"fmt"
	"time"
)

// SimpanData ..
func SimpanData(datanya DataSerial) error {

	var temp = DataSerialDB{
		Busvoltage:   datanya.Busvoltage,
		Current:      datanya.Current,
		Humidity:     datanya.Busvoltage,
		Loadvoltage:  datanya.Busvoltage,
		Shuntvoltage: datanya.Busvoltage,
		Temperature:  datanya.Busvoltage,
		UpdateAt:     time.Now(),
	}
	// db.Create(&animal)
	errr := KonDB.Create(&temp).GetErrors()
	if len(errr) > 0 {
		fmt.Println(errr)
		var stringErr string
		for index := 0; index < len(errr); index++ {
			stringErr = stringErr + "," + string(errr[index].Error())
		}
		return errors.New(stringErr)
	}
	return nil
}

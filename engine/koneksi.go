package engine

import (
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

//  KonDB ....
var KonDB *gorm.DB

type DataSerialDB struct {
	ID           uint64 `gorm:"primary_key"`
	Temperature  float32
	Humidity     float32
	Current      float32
	Shuntvoltage float32
	Busvoltage   float32
	Loadvoltage  float32
	CreatedAt    time.Time
	UpdateAt     time.Time
}

// BukaDatabase ..
func BukaDatabase() error {
	db, err := gorm.Open("sqlite3", "sukrondb.db")
	if err != nil {
		return err
	}
	if db.HasTable(&DataSerialDB{}) == false {
		db.AutoMigrate(&DataSerialDB{})
	}
	KonDB = db
	return nil
}

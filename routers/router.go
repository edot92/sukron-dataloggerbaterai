package routers

import (
	"github.com/astaxie/beego"
	"gitlab.com/edot92/sukron-dataloggerbaterai/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
}
